module cz.vse {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires javafx.web;

    exports cz.vse;
    opens cz.vse to javafx.fxml;
    exports cz.vse.client;
    opens cz.vse.client to javafx.fxml;
    exports cz.vse.ui;
    opens cz.vse.ui to javafx.fxml;

}