package cz.vse.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.List;
import java.util.logging.*;

public class Client extends Application {
    private static final Logger logger = Logger.getLogger(Client.class.getName());
    private ClientService clientService;
    private Stage primaryStage;
    private ObservableList<String> playerList;

    private static Client instance;

    public Client() {
        instance = this;
        playerList = FXCollections.observableArrayList();
    }

    /**
     * Returns the singleton instance of the Client.
     *
     * @return the singleton instance.
     */
    public static Client getInstance() {
        return instance;
    }

    /**
     * Returns the observable list of players.
     *
     * @return the observable list of players.
     */
    public ObservableList<String> getPlayerList() {
        return playerList;
    }

    /**
     * Returns the ClientService instance.
     *
     * @return the ClientService instance.
     */
    public ClientService getClientService() {
        return clientService;
    }

    /**
     * Starts the client application.
     *
     * @param primaryStage the primary stage for this application.
     * @throws Exception if an error occurs during startup.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;



        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                setupLogger();
                clientService = new ClientService();
                clientService.initialize("localhost", 48620);
                clientService.setPlayerListUpdateHandler(Client.this::updatePlayerList);
                clientService.receive();
                return null;
            }
        };

        task.setOnSucceeded(event -> {
            try {
                showLoginScreen();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Error showing login screen on thread: " + Thread.currentThread().getName(), e);
            }
        });

        new Thread(task).start();

        primaryStage.setOnCloseRequest((WindowEvent event) -> {

            disconnect();
            Platform.exit();
            System.exit(0);
        });
    }
    /**
     * Disconnects the client by closing the client service.
     */
    public void disconnect() {
        clientService.send("LOGOUT");
        clientService.close();
    }

    /**
     * Sets up the logger for the application.
     *
     * @throws IOException if an error occurs during logger setup.
     */
    private void setupLogger() throws IOException {
        FileHandler fileHandler = new FileHandler("client.log");
        fileHandler.setFormatter(new SimpleFormatter());
        fileHandler.setLevel(Level.ALL);
        logger.addHandler(fileHandler);
        logger.log(Level.INFO, "Logger setup complete on thread: " + Thread.currentThread().getName());
    }

    /**
     * Updates the player list with the given players.
     *
     * @param players the list of players to update.
     */
    private void updatePlayerList(List<String> players) {
        playerList.setAll(players);
    }

    /**
     * Shows the login screen.
     *
     * @throws IOException if an error occurs during loading the login screen.
     */
    public void showLoginScreen() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("LoginScreen.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        logger.log(Level.INFO, "Login screen displayed on thread: " + Thread.currentThread().getName());
    }

    /**
     * Shows the player list screen.
     *
     * @throws IOException if an error occurs during loading the player list screen.
     */
    public void showPlayerListScreen() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("PlayerList.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Player List");
        primaryStage.setScene(new Scene(root));
        clientService.send("LIST");
        primaryStage.show();
        logger.log(Level.INFO, "Player list screen displayed on thread: " + Thread.currentThread().getName());
    }

    /**
     * Shows the game screen.
     *
     * @throws IOException if an error occurs during loading the game screen.
     */
    public void showGameScreen() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("GameUI.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Tic-Tac-Toe");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        logger.log(Level.INFO, "Player list screen displayed on thread: " + Thread.currentThread().getName());
    }

    /**
     * Sends a message to the server.
     *
     * @param message the message to send.
     */
    public void sendMessage(String message) {
        clientService.send(message);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
