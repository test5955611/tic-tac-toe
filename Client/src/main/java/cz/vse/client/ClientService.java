package cz.vse.client;

import cz.vse.ui.GameUIController;
import cz.vse.ui.LoginUIController;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientService {
    private static final Logger logger = Logger.getLogger(ClientService.class.getName());
    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;
    private final ExecutorService executorService;
    private final BlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();

    private Consumer<List<String>> playerListUpdateHandler;
    private Consumer<String> gameUpdateHandler;
    private boolean initialized = false;
    private Client client = Client.getInstance();

    /**
     * Constructor for the ClientService class.
     * Initializes the executor service for handling asynchronous tasks.
     */
    public ClientService() {
        executorService = Executors.newCachedThreadPool();
        startMessageProcessor();
    }

    /**
     * Initializes the client service by connecting to the server.
     *
     * @param serverAddress the server address.
     * @param port the server port.
     */
    public synchronized void initialize(String serverAddress, int port) {
        if (initialized) {
            logger.log(Level.WARNING, "ClientService already initialized on thread: " + Thread.currentThread().getName());
            return;
        }
        try {
            socket = new Socket(serverAddress, port);
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
            logger.log(Level.INFO, "Client initialized: connected to server at {0}:{1} on thread: " + Thread.currentThread().getName(), new Object[]{serverAddress, port});
            initialized = true;
            receive();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error initializing client on thread: " + Thread.currentThread().getName(), e);
        }
    }

    /**
     * Sets the player list update handler.
     *
     * @param handler the player list update handler.
     */
    public void setPlayerListUpdateHandler(Consumer<List<String>> handler) {
        this.playerListUpdateHandler = handler;
    }

    /**
     * Sets the game update handler.
     *
     * @param handler the game update handler.
     */
    public void setGameUpdateHandler(Consumer<String> handler) {
        this.gameUpdateHandler = handler;
    }

    /**
     * Sends a message to the server.
     *
     * @param message the message to send.
     */
    public void send(String message) {
        executorService.submit(() -> {
            output.println(message);
            logger.log(Level.INFO, "Sent to server: " + message);
        });
    }

    /**
     * Receives messages from the server.
     */
    public void receive() {
        executorService.submit(() -> {
            try {
                String response;
                while ((response = input.readLine()) != null) {
                    messageQueue.put(response);
                }
            } catch (SocketException e) {
                if (e.getMessage().equals("Socket closed")) {
                    logger.log(Level.INFO, "Socket closed, stopping receive loop.");
                } else {
                    logger.log(Level.SEVERE, "SocketException while receiving message from server", e);
                }
            } catch (IOException | InterruptedException e) {
                logger.log(Level.SEVERE, "Error receiving message from server", e);
            }
        });
    }
    private void startMessageProcessor() {
        executorService.submit(() -> {
            try {
                while (true) {
                    String message = messageQueue.take();
                    handleServerMessage(message);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                logger.log(Level.SEVERE, "Message processor interrupted", e);
            }
        });
    }

    /**
     * Handles server messages.
     *
     * @param message the server message.
     */
    private void handleServerMessage(String message) {
        String command;
        String content;

        if (message.contains(" ")) {
            command = message.substring(0, message.indexOf(' '));
            content = message.substring(message.indexOf(' ') + 1);
        } else {
            command = message;
            content = "";
        }

        logger.log(Level.INFO, "Received from server: " + message + " on thread: " + Thread.currentThread().getName());

        switch (command) {
            case "LOGIN_SUCCESS":
                Platform.runLater(() -> {
                    try {
                        client.showPlayerListScreen();
                    } catch (IOException e) {
                        logger.log(Level.SEVERE, "Error showing player list screen on thread: " + Thread.currentThread().getName(), e);
                    }
                });
                break;
            case "LOGIN_FAILED":
                Platform.runLater(() -> LoginUIController.showAlert("Login Failed", content));
                break;
            case "LIST":
                String playersString = content;
                List<String> players = Arrays.asList(playersString.split(","));
                Platform.runLater(() -> {
                    if (playerListUpdateHandler != null) {
                        playerListUpdateHandler.accept(players);
                    }
                });
                break;
            case "GAME_START":
                Platform.runLater(() -> {
                    try {
                        client.showGameScreen();
                    } catch (IOException e) {
                        logger.log(Level.SEVERE, "Error showing game UI", e);
                    }
                });
                break;
            case "GAME_UPDATE":
            case "MOVE":
            case "MOVE_ACCEPTED":
            case "MOVE_REJECTED":
            case "WIN":
            case "LOSE":
            case "YOUR_TURN":
            case "OPPONENT_TURN":
            case "ENEMY_MOVE":
            case "REMATCH_REQUEST":
            case "GAME_END":
                Platform.runLater(() -> {
                    if (gameUpdateHandler != null) {
                        gameUpdateHandler.accept(message);
                    }
                });
                break;
            default:
                break;
        }
    }

    /**
     * Closes the client service, including socket and I/O streams.
     */
    public void close() {
        try {
            if (socket != null) socket.close();
            if (input != null) input.close();
            if (output != null) output.close();
            executorService.shutdown();
            logger.log(Level.INFO, "Client resources closed successfully on thread: " + Thread.currentThread().getName());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error closing resources on thread: " + Thread.currentThread().getName(), e);
        }
    }
}
