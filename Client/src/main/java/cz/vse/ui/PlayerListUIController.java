package cz.vse.ui;

import cz.vse.client.Client;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlayerListUIController {

    @FXML
    private ListView<String> playerListView;

    @FXML
    private Button challengeButton;
    @FXML
    private Button logoutButton;

    private ObservableList<String> playerList;

    private static final Logger logger = Logger.getLogger(PlayerListUIController.class.getName());

    private Client client = Client.getInstance();

    public void setClient(Client client) {
        this.client = client;
    }

    @FXML
    public void initialize() {
        playerList = client.getPlayerList();
        playerListView.setItems(playerList);

        client.sendMessage("LIST");
        logger.log(Level.INFO, "Initial player list request sent");
    }

    /**
     * Handles the challenge button click event.
     */
    @FXML
    private void handleChallenge() {
        String selectedPlayer = playerListView.getSelectionModel().getSelectedItem();
        if (selectedPlayer != null && !selectedPlayer.isEmpty()) {
            client.sendMessage("CHALLENGE " + selectedPlayer);
            logger.log(Level.INFO, "Challenge button clicked: " + selectedPlayer + " on thread: " + Thread.currentThread().getName());
        }
    }

    /**
     * Handles the logout button click event.
     */
    @FXML
    private void handleLogout() {
        logger.info("Logout button clicked on thread: " + Thread.currentThread().getName());
        client.disconnect();
        Platform.exit();
        System.exit(0);
    }
}
