package cz.vse.ui;

import cz.vse.client.Client;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

import java.io.IOException;

public class LoginUIController {

    @FXML
    private TextField usernameField;

    private Client client = Client.getInstance();

    public void setClient(Client client) {
        this.client = client;
    }

    @FXML
    private void initialize() {
        setClient(client);
    }

    /**
     * Handles the login button click event.
     */
    @FXML
    private void loginButtonClicked() {
        String username = usernameField.getText().trim();
        if (!username.isEmpty()) {
            client.sendMessage("LOGIN " + username);
        }
    }

    /**
     * Shows an alert with the specified title and message.
     *
     * @param title   the alert title.
     * @param message the alert message.
     */
    public static void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
