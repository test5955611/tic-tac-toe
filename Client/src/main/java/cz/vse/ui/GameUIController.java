package cz.vse.ui;

import cz.vse.client.Client;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class GameUIController {
    public Button button00, button01, button02, button10, button11, button12, button20, button21, button22;
    @FXML
    private GridPane boardGridPane;
    @FXML
    private Label statusLabel;
    @FXML
    private Button rematchButton;
    @FXML
    private Button playerListButton;

    @FXML
    private ProgressBar rematchProgressBar;

    private char[][] board = new char[3][3];
    private char currentPlayer = 'X';

    private Client client;
    private boolean myTurn;

    private ScheduledExecutorService scheduler;
    private int remainingTime;
    private static final int TURN_TIME_LIMIT = 30;

    private static final Logger logger = Logger.getLogger(GameUIController.class.getName());

    public void initialize() {
        client = Client.getInstance();
        initializeBoard();
        client.getClientService().setGameUpdateHandler(this::processServerMessage);
        rematchButton.setDisable(true);
        rematchProgressBar.setVisible(false);
        playerListButton.setDisable(true);
        scheduler = Executors.newScheduledThreadPool(1);
        logger.info("GameUIController initialized on thread: " + Thread.currentThread().getName());
    }
    /**
     * Initializes the game board.
     */
    private void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '\0';
            }
        }
        logger.info("Game board initialized on thread: " + Thread.currentThread().getName());
    }

    /**
     * Handles button click actions.
     *
     * @param event the button click event.
     */
    @FXML
    private void handleButtonAction(javafx.event.ActionEvent event) {
        Button button = (Button) event.getSource();
        Integer row = GridPane.getRowIndex(button);
        Integer col = GridPane.getColumnIndex(button);

        if (row == null || col == null) return;

        if (board[row][col] == '\0' && myTurn) {
            board[row][col] = currentPlayer;
            button.setText(String.valueOf(currentPlayer));
            button.setTextFill(currentPlayer == 'X' ? Color.RED : Color.BLUE);
            myTurn = false;
            stopTimer();
            client.sendMessage("MOVE " + (row * 3 + col));
            logger.info("Move sent: " + (row * 3 + col) + " on thread: " + Thread.currentThread().getName());
        }
    }

    /**
     * Processes messages from the server.
     *
     * @param message the message from the server.
     */
    public void processServerMessage(String message) {
        logger.info("RECEIVED FOR UI UPDATE: " + message + " on thread: " + Thread.currentThread().getName());

        Platform.runLater(() -> {
            String[] parts = message.split(" ");
            String command = parts[0];
            logger.info("UPDATING UI FROM: " + message + " on thread: " + Thread.currentThread().getName());

            switch (command) {
                case "YOUR_TURN":
                    myTurn = true;
                    statusLabel.setText("Your turn!");
                    startTimer();
                    break;
                case "OPPONENT_TURN":
                    myTurn = false;
                    statusLabel.setText("Opponent's turn...");
                    stopTimer();
                    break;
                case "MOVE_ACCEPTED":
                    statusLabel.setText("Move accepted. Waiting for opponent...");
                    stopTimer();
                    break;
                case "MOVE_REJECTED":
                    statusLabel.setText("Invalid move. Try again.");
                    break;
                case "WIN":
                    myTurn = false;
                    statusLabel.setText("You win!");
                    stopTimer();
                    break;
                case "LOSE":
                    myTurn = false;
                    statusLabel.setText("You lose!");
                    stopTimer();
                    break;
                case "TIE":
                    myTurn = false;
                    statusLabel.setText("It's a tie!");
                    stopTimer();
                    break;
                case "REMATCH_REQUEST":
                    statusLabel.setText("Rematch?");
                    rematchButton.setDisable(false);
                    rematchProgressBar.setVisible(true);
                    animateRematchButton();
                    break;
                case "GAME_END":
                    myTurn = false;
                    statusLabel.setText("Game over.");
                    rematchButton.setDisable(true);
                    rematchProgressBar.setVisible(false);
                    playerListButton.setDisable(false);
                    stopTimer();
                    break;
                case "MOVE":
                case "ENEMY_MOVE":
                    if (command.equals("ENEMY_MOVE")) {
                        int move = Integer.parseInt(parts[1]);
                        int row = move / 3;
                        int col = move % 3;
                        char player = currentPlayer == 'X' ? 'O' : 'X';
                        board[row][col] = player;
                        Button button = getButtonAt(row, col);
                        if (button != null) {
                            button.setText(String.valueOf(player));
                            button.setTextFill(player == 'X' ? Color.RED : Color.BLUE);
                        }
                    } else {
                        int move = Integer.parseInt(parts[1]);
                        char player = parts[2].charAt(0);
                        int row = move / 3;
                        int col = move % 3;
                        board[row][col] = player;
                        Button button = getButtonAt(row, col);
                        if (button != null) {
                            button.setText(String.valueOf(player));
                            button.setTextFill(player == 'X' ? Color.RED : Color.BLUE);
                        }
                    }
                    break;
            }
        });
    }

    /**
     * Gets the button at the specified row and column on the game board.
     *
     * @param row the row index.
     * @param col the column index.
     * @return the button at the specified row and column, or null if not found.
     */
    private Button getButtonAt(int row, int col) {
        for (javafx.scene.Node node : boardGridPane.getChildren()) {
            if (GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == col) {
                return (Button) node;
            }
        }
        return null;
    }

    /**
     * Shows an alert with the specified title and message.
     *
     * @param title   the alert title.
     * @param message the alert message.
     */
    private void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    @FXML
    private void handleRematchButtonAction() {
        sendRematchRequest();
    }

    @FXML
    private void handlePlayerListButtonAction() throws IOException {
        client.sendMessage("JOIN");
        client.showPlayerListScreen();
    }

    public void sendRematchRequest() {
        client.sendMessage("REMATCH");
    }

    public void disconnect() {
        client.sendMessage("LOGOUT");
    }

    /**
     * Starts the turn timer.
     */
    private void startTimer() {
        remainingTime = TURN_TIME_LIMIT;
        scheduler.scheduleAtFixedRate(() -> {
            Platform.runLater(() -> {
                if (remainingTime > 0) {
                    statusLabel.setText("Your turn! Time left: " + remainingTime + " seconds");
                    remainingTime--;
                } else {
                    statusLabel.setText("Time's up!");
                    stopTimer();
                }
            });
        }, 0, 1, TimeUnit.SECONDS);
    }

    /**
     * Stops the turn timer.
     */
    private void stopTimer() {
        scheduler.shutdownNow();
        scheduler = Executors.newScheduledThreadPool(1);
    }
    private void animateRematchButton() {
        rematchProgressBar.setProgress(1.0);

        Timeline timeline = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(rematchProgressBar.progressProperty(), 1)),
                new KeyFrame(Duration.millis(10000), new KeyValue(rematchProgressBar.progressProperty(), 0))
        );

        timeline.setOnFinished(event -> {
            rematchButton.setDisable(true);
            rematchProgressBar.setVisible(false);
        });

        timeline.play();
    }
}
