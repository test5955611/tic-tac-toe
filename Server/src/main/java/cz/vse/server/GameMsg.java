package cz.vse.server;

import java.sql.SQLOutput;

public class GameMsg {
        private final ClientHandler entity;
        private final String message;


        public GameMsg(ClientHandler entity, String message) {
            this.entity = entity;
            this.message = message;
        }

        public ClientHandler getEntity() {
            return entity;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public String toString() {
            return "EntityMessage{entity=" + entity + ", message='" + message + "'}";
        }
    }


