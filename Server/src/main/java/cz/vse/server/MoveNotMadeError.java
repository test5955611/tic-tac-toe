package cz.vse.server;

public class MoveNotMadeError extends Exception{
    public MoveNotMadeError() {
        super("Move not made");
    }
}
