package cz.vse.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.vse.server.TTTServer.parseMsg;

public class ClientHandler extends Thread {
    private static final Logger logger = Logger.getLogger(ClientHandler.class.getName());
    private final Socket clientSocket;
    public static final ArrayList<ClientHandler> clients = new ArrayList<>();
    private static final Deque<ClientHandler> waitingPlayers = new ArrayDeque<>();
    public Game game = null;
    private BufferedReader bufferedReader;
    private PrintWriter printWriter;
    public String username = null;

    public PrintWriter getPrintWriter() {
        return printWriter;
    }

    /**
     * Konstruktor třídy ClientHandler.
     * Inicializuje vstupní a výstupní proudy pro klientův soket.
     *
     * @param clientSocket Soket klienta.
     */
    public ClientHandler(Socket clientSocket) {
        try {
            this.clientSocket = clientSocket;
            this.bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            this.printWriter = new PrintWriter(clientSocket.getOutputStream(), true);

        } catch (IOException e) {
            closeConnection();
            throw new RuntimeException(e);
        }
    }
    /**
     * Přepisuje metodu run třídy Thread.
     * Zpracovává příkazy klienta - LOGIN, JOIN, DEQUEUE, CHALLENGE, LOGOUT a LIST.
     */
    @Override
    public void run() {
        try {
            registerAttempt();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if ("LOGOUT".equals(parseMsg(line)[0])) {
                    break;
                }
                if (game != null) {
                    game.addMessage(new GameMsg(this, line));
                }
                if (this.game == null) {
                    if ("JOIN".equals(parseMsg(line)[0]) && !waitingPlayers.contains(this)) {
                        synchronized (waitingPlayers) {
                            waitingPlayers.addLast(this);
                        }
                        sendQueueAll();
                    }
                    if ("DEQUEUE".equals(parseMsg(line)[0])) {
                        removeFromQueue(this);
                        sendQueueAll();
                    }
                    if ("CHALLENGE".equals(parseMsg(line)[0])) {
                        try {
                            String username = parseMsg(line)[1];

                            if (username.equals(this.username)) {
                                logger.log(Level.SEVERE, "CLIENT CHALLENGED HIM/HERSELF.");
                                continue;
                            }

                            ClientHandler opponent = null;
                            synchronized (waitingPlayers) {
                                for (ClientHandler client : waitingPlayers) {
                                    if (client.username.equals(username)) {
                                        opponent = client;
                                    }
                                }
                            }
                            if (opponent != null) {
                                game = new Game();
                                game.setPlayer1(this);
                                game.setPlayer2(opponent);
                                Thread gameThread = new Thread(game);
                                gameThread.start();
                                removeFromQueue(this);
                                removeFromQueue(opponent);
                                sendQueueAll();
                            } else {
                                logger.log(Level.SEVERE, "CLIENT CHALLENGING A PLAYER THAT IS NOT IN THE WAITING QUEUE.");
                            }
                        } catch (ArrayIndexOutOfBoundsException e) {
                            logger.log(Level.SEVERE, "CLIENT DID NOT SEND A USERNAME WITH CHALLENGE COMMAND");
                            continue;
                        }
                    }
                    if ("LIST".equals(parseMsg(line)[0])) {
                        sendQueue(this);
                    }
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error handling client", e);
        } finally {
            if (game != null) {
                game.addMessage(new GameMsg(this, "LEAVE"));
            }
            closeConnection();
        }
    }
    /**
     * Metoda pro odstranění klienta z fronty čekajících hráčů.
     *
     * @param handler Odebíraný klient.
     */
    private void removeFromQueue(ClientHandler handler) {
        synchronized (waitingPlayers) {
            waitingPlayers.remove(handler);
        }
    }
    /**
     * Metoda pro registraci klienta.
     *
     * @throws IOException Pokud dojde k chybě při čtení z bufferu.
     */
    private void registerAttempt() throws IOException {
        while (true) {
            String line = bufferedReader.readLine();
            if ("LOGIN".equals(parseMsg(line)[0])) {
                try {
                    String username = parseMsg(line)[1].trim();
                    if (username.isEmpty()) {
                        this.printWriter.println("LOGIN_FAILED Enter your username.");
                        continue;
                    }
                    boolean isTaken = false;
                    synchronized (clients) {
                        for (ClientHandler client : clients) {
                            if (client.username.equals(username)) {
                                this.printWriter.println("LOGIN_FAILED Username already taken.");
                                logger.log(Level.INFO, "LOGIN FAILED USERNAME TAKEN");

                                isTaken = true;
                            }
                        }
                        if (!isTaken) {
                            this.username = username;
                            clients.add(this);
                            this.printWriter.println("LOGIN_SUCCESS " + username);

                            logger.log(Level.INFO, "NEW USER ADDED");
                            synchronized (waitingPlayers) {
                                waitingPlayers.addLast(this);
                            }
                            sendQueueAll();
                            break;
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    logger.log(Level.SEVERE, "LOGIN FAILED. CLIENT DID NOT SEND A USERNAME WITH LOGIN COMMAND");
                }
            } else {
                logger.log(Level.SEVERE, "INVALID COMMAND FOR LOGIN.");
            }
        }
    }

    /**
     * Metoda pro uzavření spojení s klientem.
     */
    public void closeConnection() {
        logger.log(Level.INFO, "CLIENT LEFT");
        synchronized (clients) {
            clients.remove(this);
        }
        removeFromQueue(this);
        sendQueueAll();
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (printWriter != null) {
                printWriter.close();
            }
            if (clientSocket != null) {
                clientSocket.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Metoda pro odeslání názvů čekajících hráčů všem klientům.
     */
    private void sendQueueAll() {
        synchronized (ClientHandler.clients) {
            for (ClientHandler client : ClientHandler.clients) {
                if (client.game != null) continue;
                sendQueue(client);
            }
        }
    }

    /**
     * Metoda pro odeslání názvů čekajících hráčů klientovi.
     */
    private void sendQueue(ClientHandler client) {
        synchronized (waitingPlayers) {
            Deque<ClientHandler> arr = new ArrayDeque<>(waitingPlayers);
            arr.remove(client);
            String names = "";
            if (!arr.isEmpty()) {
                for (ClientHandler handler: arr) {
                    names = names + handler.username + ",";
                }
                names = names.substring(0, names.length() - 1);
            }
            String listMessage = "LIST " + names.toString();
            logger.log(Level.INFO, "Sending to "+client.username+":" + listMessage);
            client.printWriter.println(listMessage);

        }
    }
}
