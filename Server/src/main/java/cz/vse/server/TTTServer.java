package cz.vse.server;

import java.io.*;
import java.net.*;
import java.util.logging.*;

public class TTTServer {
    private static final Logger logger = Logger.getLogger(TTTServer.class.getName());
    private ServerSocket serverSocket;
    private int port;

    public TTTServer(int port) {
        this.port = port;
    }

    /**
     * Spustí server. Server čeká na připojení klientů.
     */
    public void start() {
        try {
            serverSocket = new ServerSocket(port);
            logger.info("Server started. Waiting for clients...");

            while (true) {
                Socket clientSocket = serverSocket.accept();
                logger.info("New client connected: " + clientSocket);
                new ClientHandler(clientSocket).start();
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error in server", e);
        } finally {
            try {
                if (serverSocket != null) {
                    serverSocket.close();
                }
                synchronized (ClientHandler.clients) {
                    for (ClientHandler client : ClientHandler.clients) {
                        client.closeConnection();
                    }
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Error closing server socket", e);
            }
        }
    }

    public static void main(String[] args) {
        int port = 48620;
        try {
            LogManager.getLogManager().reset();
            Logger logger = Logger.getLogger("");
            logger.setLevel(Level.ALL);

            ConsoleHandler consoleHandler = new ConsoleHandler();
            consoleHandler.setLevel(Level.ALL);
            logger.addHandler(consoleHandler);

            FileHandler fileHandler = new FileHandler("server.log");
            fileHandler.setFormatter(new SimpleFormatter());
            fileHandler.setLevel(Level.ALL);
            logger.addHandler(fileHandler);

            TTTServer server = new TTTServer(port);
            server.start();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error SERVER SHUT DOWN", e);
        }
    }

    /**
     * Rozdělí zprávu přijatou od klienta.
     *
     * @param message Zpráva přijatá od klienta.
     * @return Pole řetězců obsahující rozdělenou zprávu.
     */
    public static String[] parseMsg(String message) {
        return message.split(" ", 2);
    }
}

