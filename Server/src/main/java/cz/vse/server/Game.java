package cz.vse.server;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import static cz.vse.server.TTTServer.parseMsg;


public class Game implements Runnable {
        private static final Logger logger = Logger.getLogger(Game.class.getName());
        private final char[] board = new char[9];
        private int moves = 0;
        private int currentPlayerIndex = 0;
        private int opponentPlayerIndex = 1;
        private boolean requestRematch = false;
        private boolean[] rematch = new boolean[2];

        private ClientHandler[] handlers = new ClientHandler[2];
        private PrintWriter[] outputs = new PrintWriter[2];
        private final Deque<GameMsg> messages;
        private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        private Future<?> moveFuture;

        /**
         * Konstruktor třídy Game.
         * Inicializuje frontu zpráv a nastavuje počáteční hodnoty pro rematch.
         */
        public Game() {
            messages = new LinkedList<>();
            rematch[0] = false;
            rematch[1] = false;
            logger.log(Level.INFO, "GAME INIT", this);
        }

        /**
         * Metoda pro nastavení hráče 1.
         * Přiřazuje ClientHandlera ke hře, výstupní PrintWriter pro hráče 1 a přiradí instanci Hry třídě Clienthandler.
         *
         * @param clientHandler ClientHandler hráče 1.
         */
        public void setPlayer1(ClientHandler clientHandler) {
            handlers[0] = clientHandler;
            outputs[0] = clientHandler.getPrintWriter();
            clientHandler.game = this;
            logger.log(Level.INFO, "PLAYER 1 SET", this);
        }
        /**
         * Metoda pro nastavení hráče 2.
         * Přiřazuje ClientHandlera ke hře, výstupní PrintWriter pro hráče 2 a přiradí instanci Hry třídě Clienthandler
         *
         * @param clientHandler ClientHandler hráče 2.
         */
        public void setPlayer2(ClientHandler clientHandler){
            handlers[1] = clientHandler;
            outputs[1] = clientHandler.getPrintWriter();
            clientHandler.game = this;
            logger.log(Level.INFO, "PLAYER 2 SET", this);
        }

        /**
         * Metoda run přepisuje metodu z rozhraní Runnable.
         * Zpracovává herní akce, dokud jsou oba hráči připojeni.
         */
        @Override
        public void run() {
            logger.log(Level.INFO, "GAME STARTED", this);
            try {
                initClientMsgs();
                setTimer();
                while (playersConnected()) {
                    GameMsg msg = this.pollMessage();
                    ClientHandler messenger = msg.getEntity();
                    String cmd = msg.getMessage();

                    if ("LEAVE".equals(parseMsg(cmd)[0])) {
                        int leaverIndex = findClientHandlerIndex(messenger);
                        int opponentIndex = leaverIndex == 0 ? 1 : 0;
                        removePlayer(leaverIndex);
                        if (!requestRematch) {
                            outputs[opponentIndex].println("WIN Opponent left the game.");
                        }
                    }
                    if (!requestRematch) {
                        if ("MOVE".equals(parseMsg(cmd)[0])) {
                            if (messenger == handlers[currentPlayerIndex]) {
                                try {
                                    PrintWriter currentOutput = outputs[currentPlayerIndex];

                                    int move = Integer.parseInt(parseMsg(cmd)[1]);

                                    if (board[move] != '\0') {
                                        currentOutput.println("MOVE_REJECTED Invalid move. Try again.");
                                        continue;
                                    }

                                    moveFuture.cancel(true);
                                    board[move] = (currentPlayerIndex == 0) ? 'X' : 'O';
                                    moves++;
                                    currentOutput.println("MOVE_ACCEPTED");
                                    outputs[opponentPlayerIndex].println("ENEMY_MOVE " + move);
                                    if (checkWin()) {
                                        currentOutput.println("WIN");
                                        outputs[opponentPlayerIndex].println("LOSE");
                                        requestRematch = true;
                                        askForRematch();
                                    } else if (moves >= 9) {
                                        outputs[0].println("TIE");
                                        outputs[1].println("TIE");
                                        askForRematch();
                                    }

                                    if (requestRematch) {
                                        continue;
                                    }

                                    currentPlayerIndex = (currentPlayerIndex + 1) % 2;
                                    opponentPlayerIndex = (currentPlayerIndex + 1) % 2;
                                    outputs[currentPlayerIndex].println("YOUR_TURN");
                                    outputs[opponentPlayerIndex].println("OPPONENT_TURN");
                                    setTimer();
                                } catch (ArrayIndexOutOfBoundsException e) {
                                    logger.log(Level.SEVERE, "CLIENT DID NOT SEND A NUMBER WITH MOVE COMMAND");
                                }
                            } else {
                                outputs[opponentPlayerIndex].println("MOVE_REJECTED Not your turn.");
                            }
                        }
                    }
                    if ("REMATCH".equals(parseMsg(cmd)[0]) && requestRematch) {
                        rematch[findClientHandlerIndex(messenger)] = true;
                        if (rematch()) {
                            resetGame();
                        }
                    }
                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "GAME CRASHED", e);
            } finally {
                outputs[0].println("GAME_END");
                outputs[1].println("GAME_END");
                kill();
                logger.log(Level.INFO, "GAME ENDED", this);
            }
        }

    /**
     * Pošle zprávy zahájení hry hráčům.
     */
    private void initClientMsgs() {
        outputs[0].println("GAME_START");
        outputs[0].println("YOUR_TURN");
        outputs[1].println("GAME_START");
        outputs[1].println("OPPONENT_TURN");
    }
    /**
     * Kontroluje, zda jsou oba hráči připojeni.
     *
     * @return True, pokud jsou oba hráči připojeni, jinak False.
     */
    private boolean playersConnected() {
        return handlers[0] != null && handlers[1] != null;
    }
    /**
     * Kontroluje, zda oba hráči chtějí rematch.
     *
     * @return True, pokud oba hráči chtějí rematch, jinak False.
     */
    private boolean rematch() {
        synchronized (rematch) {
            return rematch[0] && rematch[1];
        }
    }
    /**
     * Nastaví časovač pro tah hráče.
     */
    private void setTimer() {
        moveFuture = executor.schedule(() -> {
                outputs[currentPlayerIndex].println("LOSE");
                outputs[opponentPlayerIndex].println("WIN Opponent did not make a move on time.");
                kill();
                addMessage(new GameMsg(null, "end"));
        }, 30, TimeUnit.SECONDS);
    }
    /**
     * Kontroluje, zda byla dosažena výhra.
     *
     * @return True, pokud byla dosažena výhra, jinak False.
     */
    private boolean checkWin() {
            int[][] winPatterns = {
                    {0, 1, 2}, {3, 4, 5}, {6, 7, 8},
                    {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
                    {0, 4, 8}, {2, 4, 6}
            };

            for (int[] pattern : winPatterns) {
                if (board[pattern[0]] != '\0' &&
                        board[pattern[0]] == board[pattern[1]] &&
                        board[pattern[0]] == board[pattern[2]]) {
                    return true;
                }
            }
            return false;
        }
        /**
         * Spustí proces vyžádání rematche.
         */
        private void askForRematch() {
            requestRematch = true;
            outputs[0].println("REMATCH_REQUEST");
            outputs[1].println("REMATCH_REQUEST");
            new Thread(() -> {
                long startTime = System.currentTimeMillis();
                long duration = 10_000;

                while (System.currentTimeMillis() - startTime < duration && this.playersConnected()) {
                    if (rematch()) {
                        return;
                    }
                }
                kill();
                addMessage(new GameMsg(null, "end"));
            }).start();
        }
        /**
         * Resetuje stav hry pro novou hru.
         */
        private void resetGame() {
            this.initClientMsgs();
            Arrays.fill(board, '\0');
            moves = 0;
            currentPlayerIndex = 0;
            opponentPlayerIndex = 1;
            requestRematch = false;
            rematch[0] = false;
            rematch[1] = false;
            setTimer();
        }
        /**
         * Ukončí hru.
         */
        private void kill() {
            removePlayer(0);
            removePlayer(1);
            moveFuture.cancel(true);
            executor.shutdown();
        }
    /**
     * Odstraní hráče ze hry.
     *
     * @param index Index hráče, který se má odstanit.
     */
    private void removePlayer(int index) {
        if (handlers[index] != null) {
            handlers[index].game = null;
            handlers[index] = null;
        }
    }

    /**
     * Přidá zprávu do fronty zpráv k odeslání.
     *
     * @param message Zpráva k přidání.
     */
    public void addMessage(GameMsg message) {
        synchronized (messages) {
            messages.add(message);

            logger.info("Pridali jsme zpravu do fronty k odeslani, vlakno se probouzi.");
            messages.notifyAll();
        }
    }
    /**
     * Odebere a vrátí zprávu z fronty zpráv.
     *
     * @return Přijatá zpráva.
     * @throws InterruptedException Pokud je čekající vlákno přerušeno.
     */
    public GameMsg pollMessage() throws InterruptedException {
        synchronized (messages) {
            while (messages.isEmpty()) {
                logger.info("Fronta zprav k odeslani je prazdna, vlakno ceka.");
                messages.wait();
            }
            return messages.poll();
        }
    }

    /**
     * Najde a vrátí index ClientHandlera v poli hráčů.
     *
     * @param target Hledaný ClientHandler.
     * @return Index hledaného ClientHandlera.
     */
    private int findClientHandlerIndex(ClientHandler target) {
        int i;
        for (i = 0; i < handlers.length; i++) {
            if (handlers[i].equals(target)) {
                 break;
            }
        }
        return i;
    }
}
